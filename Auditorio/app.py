# -*- coding: utf8 -*-
import random
import sys

#creamos una funcion para hacer las operaciones
def Main():
    #abrimos el archivo de entrada
    entrada = open('entrada.in','r')
    #leemos los datos de las lineas y reemplazamos el fin de linea para poder leer de manera correcta los datos
    datos = entrada.read().replace('\n','' )
    # Cerramos el fichero.
    entrada.close()
    #evaluamos los datos
    arr = eval('datos')
    #asignamos la variable "array" a la evaluacion de los datos
    array = arr
    #dividimos el array, separando cada elemento para trabajarlo de manera individual
    datos = array.split()
    #rescatamos el primer numero del nuevo array y lo comparamos con el largo del archivo restante
    #Esto es para comprobar que el primer número (alumnos) es igual al número de estaturas dadas en el archivo
    PrimerElemento = int(datos[0])
    LargoArray = len(datos) - 1
    #comparamos los elementos
    if PrimerElemento == LargoArray:
        print "\nMensaje: Formato de archivo correcto\n"
    else:
        print "\nEl número de alumnos tiene que ser igual al número de estaturas. Por favor, revise su archivo de entrada\n"
        sys.exit()
    #Ordenamos el array para ordenar los números en caso de que estén desordenados
    DatosOrdenados = sorted(datos)
    #comprobamos que las estaturas estén en el rango de 80-190
    for i in DatosOrdenados[1:]:
        valor = int(i)
        if valor < 80 or valor > 190:
            print "\nMensaje: Una o más estaturas están fuera del rango (80 - 190). Por favor, revise su archivo de entrada\n"
            sys.exit()
    #recorremos el array y comparamos cada elemento con el siguiente en la lista
    #si son distintos, agregamos 1 a la fila
    #declaramos el contador de filas
    filas = 0
    for x,y in zip(DatosOrdenados,DatosOrdenados[1:]):
      if x != y:
         filas = filas + 1
    #creamos una variable para guardar la lista de números repetidos
    lista = []
    #recorremos el array para buscar los números repetidos, que corresponden al número de asientos en cada fila
    for numero in DatosOrdenados:
        lista.append((DatosOrdenados.count(numero)))
    #buscamos el número mayor de repeticiones y los asignamos como asientos
    asientos = max(lista)
    #guardamos los datos "filas" y "asientos" para preparar la salida
    salida = "%s %s" %(filas, asientos)
    #retornamos la variable salida
    return salida

#creamos un archivo
def CrearArchivo():
    salida = open('salida.out','w')
    salida.close()

#creamos una función para escribir sobre el archivo
def EscribirArchivo():
    #instanciamos la funcion principal
    leer = Main()
    salida = open('salida.out','a')
    #escribimos la variable de salida
    salida.write(leer)
    salida.close()

#hacemos el llamado a cada función para que se ejecuten
Main()
CrearArchivo()
EscribirArchivo()
