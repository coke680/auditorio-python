# Auditorio #

Esta es una serie de algoritmos que resuelve el problema de ordenar alumnos en un auditorio por estatura, optimizando el número de filas para ello.

* Realizado en Python
* Hecho en 3 versiones
* Cuenta con los archivos de entrada y también, algunos ejemplos de salida.