# -*- coding: utf8 -*-
import random
import sys

with open('entrada.in','r') as f:
    print "\nLeyendo datos"
    archivo=f.readlines()
f.close()
#guardamos los elementos en un arreglo
arreglo=eval('archivo')
print "Trabajando archivo"
archivo=arreglo[1].split()
#transformamos a entero el primer elemento de arreglo
primero=int(arreglo[0])
largo=len(archivo)
#vemos si el numero de alumnos es igual al numero de alturas
print "Comparando datos"
if primero==largo:
    #declaramos el contador de filas y la variable de frecuencia de cada altura
    fila=1
    frecuencia=[]
    #ordenamos los datos en caso de que estén en forma aleatoria
    print "Ordenando arreglo"
    orden=sorted(archivo)
    #Vemos si las alturas no exceden el rango 
    for numero in orden[1:]:
        altura=int(numero)
        if altura < 80 or altura > 190:
            print "\nError: Las alturas tienen que estar entre 80 y 190\n"
            sys.exit()
    #recorremos el arreglo y comparamos elementos uno a uno
    print "Calculando..."
    for x,y in zip(orden,orden[1:]):
      if x!=y:
         fila=fila+1
    #recorremos el arreglo para buscar los números repetidos, que corresponden al número de asientos en cada fila
    for numero in orden:
        frecuencia.append((orden.count(numero)))
    #buscamos la mayor frecuencia
    NumeroAsientos=max(frecuencia)

    #asignamos "fila" y "NumeroAsientos" a auditorio
    auditorio="%s %s" %(fila, NumeroAsientos)    
    print "Creando y guardando datos"
    f=open('salida.out','w')
    f.close()
    f=open('salida.out','a')
    f.write(auditorio)
    f.close()
    print "¡Listo!\n"
#si no coincide, termina el programa
else:
    print "\nError: El número de alumnos debe ser igual al número de alturas\n"
    sys.exit()


