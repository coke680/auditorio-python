# -*- coding: utf8 -*-
"""
Algoritmo para determinar el máximo de filas y asientos de un auditorio, ordenando previamente a los alumnos de menor a mayor estatura.
"""

print(__doc__)
import random
import sys

#declaramos el contador de filas y la variable de repeticiones de cada estatura
numero_filas = 1
repeticiones = [] 
#abrimos el archivo de entrada en modo lectura
f = open('entrada.in','r')
#extraemos la primera linea del archivo
print "Extrayendo datos"
primera_linea = f.readline()
#extraemos la segunda linea del archivo
segunda_linea = f.readline()
#cerramos el archivo
f.close()

#guardamos los elementos que fueron extraidos del archivo en 2 arreglos y los evaluamos
array_1 = eval('primera_linea')
array_2 = eval('segunda_linea')
#convertimos el primer digito a entero
primera_linea = int(array_1)
#la segunda linea la dividimos en elementos y calculamos el numero de elementos que existen
segunda_linea = array_2.split()
largo_segunda_linea = len(segunda_linea)
#ordenamos los elementos de menos a mayor
ordenar = sorted(segunda_linea)

#RESTRICCIONES DEL PROBLEMA
print "Revisando restricciones"
#comparamos el numero de alumnos y el numero de estaturas
if primera_linea == largo_segunda_linea:       
    #Vemos si las estaturas no exceden el rango entre 80 y 190
    for i in ordenar[0:]:
    	#guardamos los numeros como enteros
        estatura = int(i)
        if (estatura < 80) or (estatura > 190):
            print "ERROR => Las estaturas de los alumnos tienen que estar entre 80 y 190\n"
            sys.exit()
else:
    print "ERROR => Numero de alumnos y estaturas no corresponden\n"
    sys.exit()
print "Calculando salida"
#comparamos los elementos uno a uno para ver cuántos tipos de estatura existen
for x,y in zip(ordenar,ordenar[1:]):
  if x != y:
  	#guardamos el numero de estaturas presentes
     numero_filas = numero_filas + 1
#recorremos el arreglo para buscar los números repetidos, que corresponden al número de asientos en cada numero_filas
for i in ordenar:
    repeticiones.append((ordenar.count(i)))
#buscamos la mayor repetición de número en el archivo
asientos = max(repeticiones)

#asignamos "numero_filas" y "asientos" a archivo_final
archivo_final = "%s %s" %(numero_filas, asientos)
#creamos el archivo de salida
f = open('salida.out','w')
f.close()
#escribimos en el archivo de salida
f = open('salida.out','a')
f.write(archivo_final)
print "Salida creada: %s \n" % archivo_final
f.close()